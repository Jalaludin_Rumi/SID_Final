package inputan;

import java.sql.PreparedStatement;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import koneksi.DB_form;


public class kavling extends javax.swing.JPanel {
    public boolean databaru;
    DB_form con;
    private Object [][] Ftabel = null;
    private String [] label = {"Blok","Tipe","Luas Tanah(m2)","Harga(Rp)"};


    
    public kavling() {
        initComponents();
        this.setVisible(true);
        
        con = new DB_form();
        con.Class();
        
        BacaTabel();
        refresh();
        jbH.setEnabled(false);
        jbU.setEnabled(false);
    }
    
    public void BacaTabel(){
        try {
            con.ss = (Statement) con.cc.createStatement();
            String sql = "SELECT * FROM kavling";
            con.rs = con.ss.executeQuery(sql);
            ResultSetMetaData m = con.rs.getMetaData();
            int kolom = m.getColumnCount();
            int baris = 0;
            while (con.rs.next()) {                
                baris = con.rs.getRow();
            }
            Ftabel = new Object[baris][kolom];
            int x = 0;
            con.rs.beforeFirst();
            while (con.rs.next()) {                
                Ftabel[x][0] = con.rs.getString("blok");
                Ftabel[x][1] = con.rs.getString("tipe");
                Ftabel[x][2] = con.rs.getString("luas");
                Ftabel[x][3] = con.rs.getString("harga");
                x++;
            }
            tblKavling.setModel(new DefaultTableModel(Ftabel,label));
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }
    
    private void baru(){
        databaru=true;
        // mengosongkan textbox
        tBlok.setText("");
        tTipe.setText("");
        tLuas.setText("");
        tHarga.setText("");
        jbS.setEnabled(true);
        jbU.setEnabled(false);
        jbH.setEnabled(false);
    }
    private void refresh(){
       
    }
    
    private void simpan(){
            String blok = tBlok.getText();
            String tipe = tTipe.getText();
            String luas = tLuas.getText();
            String harga = tHarga.getText();
        try{
            con = new DB_form();
            con.Class();
            String sql = "INSERT INTO kavling VALUES (?,?,?,?)";
            PreparedStatement p = (com.mysql.jdbc.PreparedStatement) con.cc.prepareStatement(sql);
            p.setString(1, blok);
            p.setString(2, tipe);
            p.setString(3, luas);
            p.setString(4, harga);
            p.executeUpdate();
            Object[] options = {"OK"};
            int input = JOptionPane.showOptionDialog(null,
                    "Data Sudah Disimpan", "Info",
                    JOptionPane.PLAIN_MESSAGE,
                    JOptionPane.QUESTION_MESSAGE,
                    null,
                    options,
                    options[0]);
            if (input == JOptionPane.OK_OPTION) {
                baru();
                BacaTabel();
                refresh();
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Data Berhasil Di Simpan");
        }   
    }
    
    private void ubah(){
        String blok = this.tBlok.getText();
        String tipe = tTipe.getText();
        String luas = tLuas.getText();
        String harga = tHarga.getText();
        try {
            con = new DB_form();
            con.Class();
            String sql = "UPDATE kavling SET tipe=?, luas=?, harga=? WHERE blok=? ";
            PreparedStatement p = (com.mysql.jdbc.PreparedStatement) con.cc.prepareStatement(sql);
            p.setString(1, tipe);
            p.setString(2, luas);
            p.setString(3, harga);
            p.setString(4, blok);
            p.executeUpdate();
            con.cc.close();
            
            Object[] options = {"OK"};
            int input = JOptionPane.showOptionDialog(null,
                    "Data Sudah Diubah", "Info",
                    JOptionPane.PLAIN_MESSAGE,
                    JOptionPane.QUESTION_MESSAGE,
                    null,
                    options,
                    options[0]);
            if (input == JOptionPane.OK_OPTION) {
                baru();
                BacaTabel();
                tBlok.setEditable(true);
                jbS.setEnabled(true);
                refresh();
            }
            
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Data Belum Lengkap");
        }
    }
    
    private void hapus(){
        try {
            con = new DB_form();
            con.Class();
            con.ss = (Statement) con.cc.createStatement();
            String sql = "DELETE FROM kavling WHERE blok='"+ tBlok.getText() +"' ";
            con.ss.executeUpdate(sql);
            con.ss.close();
            
            Object[] options = {"OK"};
            int input = JOptionPane.showOptionDialog(null,
                    "Data Sudah Dihapus!", "Info",
                    JOptionPane.PLAIN_MESSAGE,
                    JOptionPane.QUESTION_MESSAGE,
                    null,
                    options,
                    options[0]);
            if (input == JOptionPane.OK_OPTION) {
                baru();
                BacaTabel();
                tBlok.setEditable(true);
                jbS.setEnabled(true);
                refresh();
            }
            
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }
    
   
    

   
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        tBlok = new javax.swing.JTextField();
        tTipe = new javax.swing.JTextField();
        tLuas = new javax.swing.JTextField();
        tHarga = new javax.swing.JTextField();
        jbB = new javax.swing.JButton();
        jbS = new javax.swing.JButton();
        jbU = new javax.swing.JButton();
        jbH = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblKavling = new javax.swing.JTable();

        setBackground(new java.awt.Color(29, 33, 83));
        setEnabled(false);
        setPreferredSize(new java.awt.Dimension(900, 560));

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        jLabel5.setFont(new java.awt.Font("Verdana", 1, 14)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(0, 0, 0));
        jLabel5.setText("Input Data Kavling");

        jLabel6.setFont(new java.awt.Font("Verdana", 0, 12)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(0, 0, 0));
        jLabel6.setText("Blok ");

        jLabel7.setFont(new java.awt.Font("Verdana", 0, 12)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(0, 0, 0));
        jLabel7.setText("Tipe");

        jLabel8.setFont(new java.awt.Font("Verdana", 0, 12)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(0, 0, 0));
        jLabel8.setText("Luas Tanah");

        jLabel9.setFont(new java.awt.Font("Verdana", 0, 12)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(0, 0, 0));
        jLabel9.setText("Harga");

        tBlok.setBackground(new java.awt.Color(29, 33, 83));
        tBlok.setFont(new java.awt.Font("Verdana", 0, 12)); // NOI18N
        tBlok.setForeground(new java.awt.Color(255, 255, 255));

        tTipe.setBackground(new java.awt.Color(29, 33, 83));
        tTipe.setFont(new java.awt.Font("Verdana", 0, 12)); // NOI18N
        tTipe.setForeground(new java.awt.Color(255, 255, 255));

        tLuas.setBackground(new java.awt.Color(29, 33, 83));
        tLuas.setFont(new java.awt.Font("Verdana", 0, 12)); // NOI18N
        tLuas.setForeground(new java.awt.Color(255, 255, 255));

        tHarga.setBackground(new java.awt.Color(29, 33, 83));
        tHarga.setFont(new java.awt.Font("Verdana", 0, 12)); // NOI18N
        tHarga.setForeground(new java.awt.Color(255, 255, 255));

        jbB.setBackground(new java.awt.Color(29, 33, 83));
        jbB.setFont(new java.awt.Font("Verdana", 1, 12)); // NOI18N
        jbB.setForeground(new java.awt.Color(255, 255, 255));
        jbB.setText("Baru");
        jbB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbBActionPerformed(evt);
            }
        });

        jbS.setBackground(new java.awt.Color(29, 33, 83));
        jbS.setFont(new java.awt.Font("Verdana", 1, 12)); // NOI18N
        jbS.setForeground(new java.awt.Color(255, 255, 255));
        jbS.setText("Simpan");
        jbS.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbSActionPerformed(evt);
            }
        });

        jbU.setBackground(new java.awt.Color(29, 33, 83));
        jbU.setFont(new java.awt.Font("Verdana", 1, 12)); // NOI18N
        jbU.setForeground(new java.awt.Color(255, 255, 255));
        jbU.setText("Ubah");
        jbU.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbUActionPerformed(evt);
            }
        });

        jbH.setBackground(new java.awt.Color(29, 33, 83));
        jbH.setFont(new java.awt.Font("Verdana", 1, 12)); // NOI18N
        jbH.setForeground(new java.awt.Color(255, 255, 255));
        jbH.setText("Hapus");
        jbH.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbHActionPerformed(evt);
            }
        });

        tblKavling.setBackground(new java.awt.Color(29, 33, 83));
        tblKavling.setFont(new java.awt.Font("Verdana", 0, 12)); // NOI18N
        tblKavling.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tblKavling.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblKavlingMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(tblKavling);

        jScrollPane1.setViewportView(jScrollPane2);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel5)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel6)
                            .addComponent(jLabel7)
                            .addComponent(jLabel8)
                            .addComponent(jLabel9))
                        .addGap(40, 40, 40)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(tHarga)
                            .addComponent(tLuas)
                            .addComponent(tTipe)
                            .addComponent(tBlok)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jbB, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jbS, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jbU, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jbH, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 27, Short.MAX_VALUE)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 502, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(32, 32, 32)
                .addComponent(jLabel5)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(27, 27, 27)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel6)
                            .addComponent(tBlok, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(19, 19, 19)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel7)
                            .addComponent(tTipe, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(20, 20, 20)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel8)
                            .addComponent(tLuas, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(16, 16, 16)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel9)
                            .addComponent(tHarga, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(50, 50, 50)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jbB, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jbS, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jbU, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jbH, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 413, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(81, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents

    private void jbBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbBActionPerformed
        baru();
    }//GEN-LAST:event_jbBActionPerformed

    private void jbSActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbSActionPerformed
        simpan();
    }//GEN-LAST:event_jbSActionPerformed

    private void jbUActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbUActionPerformed
        ubah();
    }//GEN-LAST:event_jbUActionPerformed

    private void jbHActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbHActionPerformed
        Object[] options = {"OK"};
        int input = JOptionPane.showOptionDialog(null,
            "Apakah anda yakin?", "Peringatan",
            JOptionPane.PLAIN_MESSAGE,
            JOptionPane.QUESTION_MESSAGE,
            null,
            options,
            options[0]);
        if (input == JOptionPane.OK_OPTION) {
            hapus();

        }
    }//GEN-LAST:event_jbHActionPerformed

    private void tblKavlingMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblKavlingMouseClicked
        tBlok.setEditable(false);
        jbS.setEnabled(false);
        jbU.setEnabled(true);
        jbH.setEnabled(true);
        try {
            int index = tblKavling.getSelectedRow();
            TableModel model = tblKavling.getModel();
            tBlok.setText(model.getValueAt(index, 0).toString());
            tTipe.setText(model.getValueAt(index, 1).toString());
            tLuas.setText(model.getValueAt(index, 2).toString());
            tHarga.setText(model.getValueAt(index, 3).toString());
        } catch (Exception e) {
        }
    }//GEN-LAST:event_tblKavlingMouseClicked


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JButton jbB;
    private javax.swing.JButton jbH;
    private javax.swing.JButton jbS;
    private javax.swing.JButton jbU;
    private javax.swing.JTextField tBlok;
    private javax.swing.JTextField tHarga;
    private javax.swing.JTextField tLuas;
    private javax.swing.JTextField tTipe;
    private javax.swing.JTable tblKavling;
    // End of variables declaration//GEN-END:variables

    public void setMaximum(boolean b) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
